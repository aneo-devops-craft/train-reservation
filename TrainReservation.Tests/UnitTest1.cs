using Moq;
using System.Collections.Generic;
using System.Linq;
using TrainReservation.Domain;
using TrainReservation.Domain.ValueObjects;
using Xunit;

namespace TrainReservation.Tests
{
	public class UnitTest1
	{
		[Fact]
		public void MakeReservation_For_2_Should_Returns_A_Reservation()
		{
			var trainId = GetTrainId();
			const int seatCounts = 2;

			var mockOfITrain = Mock.Of<ITrain>();
			Mock.Get(mockOfITrain).Setup(m => m.GetAvailableSeats(trainId)).Returns(GenerateSeats().ToList());
			var sut = new TicketOffice(mockOfITrain);
			var request = new ReservationRequest(trainId, seatCounts);

			var reservation = sut.MakeReservation(request);

			Assert.Equal(trainId, reservation.TrainId);
			Assert.NotNull(reservation.BookingId);
			Assert.Equal(seatCounts, reservation.Seats.Count);

		}

		private static TrainId GetTrainId()
		{
			return new TrainId("Thib Star");
		}

		private static IEnumerable<Seat> GenerateSeats()
		{
			yield return new Seat(new CoachName("A"), 1);
			yield return new Seat(new CoachName("A"), 2);
			yield return new Seat(new CoachName("A"), 3);
			yield return new Seat(new CoachName("B"), 1);
			yield return new Seat(new CoachName("B"), 2);
			yield return new Seat(new CoachName("B"), 3);
		}
	}
}