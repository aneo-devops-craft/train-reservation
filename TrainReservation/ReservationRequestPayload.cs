﻿using System.Text.Json.Serialization;

namespace TrainReservation.API
{
    public record ReservationRequestPayload(
        [property: JsonPropertyName("train_id")] string TrainId, 
        [property: JsonPropertyName("seatcount")] int SeatCount);
}
