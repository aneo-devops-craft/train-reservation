﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace TrainReservation.API
{
    public class ReservationResponse
    {
        [JsonPropertyName("train_id")]
        public string TrainId { get; set; }
        [JsonPropertyName("booking_reference")]
        public string BookingId { get; set; }
        [JsonPropertyName("seats")]
        public List<string> Seats { get; set; }
    }
}
