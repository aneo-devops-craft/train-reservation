using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Collections.Generic;
using System.Linq;
using TrainReservation.Domain;
using TrainReservation.Domain.ValueObjects;

namespace TrainReservation.API
{
    public class Startup
    {
        class MyTrain : ITrain
        {
            public List<Seat> GetAvailableSeats(TrainId trainId)
            {
                return GenerateSeats().ToList();
            }

            private static IEnumerable<Seat> GenerateSeats()
            {
                yield return new Seat(new CoachName("A"), 1);
                yield return new Seat(new CoachName("A"), 2);
                yield return new Seat(new CoachName("A"), 3);
                yield return new Seat(new CoachName("B"), 1);
                yield return new Seat(new CoachName("B"), 2);
                yield return new Seat(new CoachName("B"), 3);
            }
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSingleton<TicketOffice>();
            services.AddSingleton<ITrain, MyTrain>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
