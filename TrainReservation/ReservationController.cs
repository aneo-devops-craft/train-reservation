﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using TrainReservation.Domain;
using TrainReservation.Domain.ValueObjects;

namespace TrainReservation.API
{
    [ApiController]
    public class ReservationController : Controller
    {
        private readonly TicketOffice office;

        public ReservationController(TicketOffice office)
        {
            this.office = office;
        }

        [HttpPost]
        [Route("/reserve")]
        public ActionResult<ReservationResponse> ReservedTicket(ReservationRequestPayload request)
        {
            // Http -> Domain
            var domainRequest = new ReservationRequest(new TrainId(request.TrainId), request.SeatCount);

            // Domain call
            
            var domainResponse = office.MakeReservation(domainRequest);

            // Domain -> Http
            var response = new ReservationResponse
            {
                BookingId = domainResponse.BookingId.Value,
                Seats = domainResponse.Seats.Select(s => $"{s.Coach.Name}{s.SeatNumber}").ToList(),
                TrainId = domainResponse.TrainId.Value
            };

            return Ok(response);
            //return Ok(new ReservationResponse() { TrainId = "train_id", BookingId = "75bcd15", Seats = new List<string> { "1A", "1B" } });
            //return Json(@"{'train_id': 'express_2000', 'booking_reference': '75bcd15', 'seats': ['1A', '1B']}");
        }
    }
}