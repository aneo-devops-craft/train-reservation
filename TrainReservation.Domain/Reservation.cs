﻿using System.Collections.Generic;
using TrainReservation.Domain.ValueObjects;

namespace TrainReservation.Domain
{
	public class Reservation
	{
		public TrainId TrainId { get; private set; }
		public BookingId BookingId { get; private set; }
		public List<Seat> Seats { get; private set; }

		public Reservation(TrainId trainId, BookingId bookingId, List<Seat> seats)
		{
			this.TrainId = trainId;
			this.BookingId = bookingId;
			this.Seats = seats;
		}
	}
}