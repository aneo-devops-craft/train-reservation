﻿using System;

namespace TrainReservation.Domain
{
	public record BookingId(string Value)
	{
		public static BookingId Create()
		{
			return new BookingId(Guid.NewGuid().ToString().Substring(0, 7));
		}
	}
}