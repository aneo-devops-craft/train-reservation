﻿namespace TrainReservation.Domain.ValueObjects
{
	public record CoachName(string Name);
}