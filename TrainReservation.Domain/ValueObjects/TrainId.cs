﻿namespace TrainReservation.Domain.ValueObjects
{
	public record TrainId(string Value);
}