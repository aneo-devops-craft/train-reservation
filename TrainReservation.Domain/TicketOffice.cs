﻿using System.Collections.Generic;
using System.Linq;
using TrainReservation.Domain.ValueObjects;

namespace TrainReservation.Domain
{
	public class TicketOffice
	{
		private readonly ITrain _train;

		public TicketOffice(ITrain train)
		{
			_train = train;
		}

		public Reservation MakeReservation(ReservationRequest request)
		{
			//TODO: implement this code!
			//What train?
			List<Seat> availableSeats = _train.GetAvailableSeats(request.TrainId);
			Reservation resevation = SelectSeat(availableSeats, request);

			return resevation;
		}

		private static Reservation SelectSeat(List<Seat> availableSeats, ReservationRequest request)
		{
			var availableCoach = GetAvailableCoach(availableSeats, request.SeatCount);
			return new Reservation(request.TrainId, BookingId.Create(), availableCoach.Take(request.SeatCount).ToList());
		}

		private static List<Seat> GetAvailableCoach(List<Seat> availableSeats, int seatCount)
		{
			return availableSeats.GroupBy(seat => seat.Coach).First(coach => coach.Count() >= seatCount).ToList();
		}
	}

	public interface ITrain
	{
		List<Seat> GetAvailableSeats(TrainId trainId);
       
	}
}