﻿using TrainReservation.Domain.ValueObjects;

namespace TrainReservation.Domain
{
	public record Seat(CoachName Coach, int SeatNumber);
}